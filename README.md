# Overview

This is an example of a namespace repo with multiple locations embedded. This could be used by a region or market

# NOTE

This is *NOT* a production or maintained repo, it is only used for workshops